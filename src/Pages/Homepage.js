import React, {Component} from "react";
import DataImporter from "../Components/DataImporter";
import {Button, Container, Header} from "semantic-ui-react";
import TreeView from "../Components/TreeView";

import "semantic-ui-css/semantic.css";
import {NavLink} from "react-router-dom";
import NavItem from "../Components/NavItem";


export default class Homepage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            secereData: []
        }
    }

    render() {
        let {secereData} = this.state;
        return (
                <div id="main-app">

                    <Container text style={{marginTop: '2em'}}>


                        {!secereData.length &&

                        <DataImporter onTextareaRef={textarea => this.textarea = textarea}
                                      onImport={(secereData) => this.setState({secereData})}/>
                        }
                    </Container>

                    <Container text style={{marginTop: '2em'}}>

                        <Header as="h3">Soy Ağacı Nedir?</Header>
                        <p>Soy ağacı bir aileye mensup olan bütün kişileri gösteren bir çeşit çizelgedir. Bunun için Arapça ağaç anlamına gelen secere kelimesi de kullanılır. Çizelge bugünden geçmişe doğru kollara ayrılarak ilerlediği için ağaca benzetilir ve bu nedenle de soy ağacı ismini alır. Bununla ilgili yapılan çalışmalar kişinin hangi soydan geldiğini bilmesi, akrabalık bağı ile ilgili haklarını öğrenebilmesi ve şayet varsa bazı ödevleri yerine getirebilmesi açısından önemlidir.</p>

                        <Header as="h3">Alt Üst Soy Neden Önemlidir?</Header>
                        <p>Bir ailenin tüm üyeleri değil de, sadece aynı soy adını taşıyan aile fertlerinin olduğu soy ağaçları ortaya çıkartılabilmektedir. Yine benzer şekilde, bir hanedanın soy ağacı çıkartılırken, yalnızca büyük ve önemli rütbelere sahip sülale üyeleri soy ağacında kendine yer bulabilmektedir. Yani, ilk bahsedilen soy ağacı türleri, soy ağacının temeldeki farklılık çeşidine ve kağıt üzerindeki yayılma cinsine göre değişmekteyken, burada anlatılan türler tamamen içerik bakımından farklılık arz etmektedir.
                        </p>


                    </Container>
                </div>
        );
    }
}

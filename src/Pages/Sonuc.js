import React, {Component} from "react";
import {Container, Header, Grid} from "semantic-ui-react";
import TreeView from "../Components/TreeView";
import NavItem from "../Components/NavItem";
import People from "../Components/People";
//localStorage.setItem("secere",JSON.stringify(data));


export default class Sonuc extends Component {

    constructor(props) {
        super(props);
        this.state = {
            secereData: []
        };

        let secere = localStorage.getItem("secere");
        if (secere) {
            this.state.secereData = JSON.parse(secere);
        }

    }


    render() {
        return ( <div className="ui text vertical center segment"
                      style={{margin: '1em 0'}}>
            <Header as='h2' className="center aligned">Ve işte aile Soyağacın.</Header>

            {this.state.secereData.length ? <TreeView secereData={this.state.secereData}/> :
                <Container text className="center aligned">
                    <People info={{me: true, isim: "Kendisi"} }/>


                    <Grid centered columns={3}>
                        <Grid.Column>
                            <People info={{me: true, isim: "Annesinin Soy Bilgileri Burada Yer Alacak"} }
                                    className="pink"/>
                        </Grid.Column>
                        <Grid.Column>
                            <People info={{
                                me: true,
                                isim: "Babasının Soy Bilgileri Burada Yer Alacak"
                            } }/>
                        </Grid.Column>
                    </Grid>
                    <br />

                    <p>
                        Aile geçmişlerini araştırmaya yeni başlayanlar için aile ağacınızı fotoğraflarla, hikayelerle ve
                        her bir kişi hakkında temel bilgilerle görselleştirmek ve paylaşmak için kolay bir yolla hemen
                        kendi alt üst soya ağacınızı oluşturun
                    </p>
                    <Header as="h3">Anasayfaya giderek sizden istenen bilgileri sağlamalısınız.</Header>

                    <NavItem color="green" style={{margin: "2em 0"}} to="/" className="ui button green ">Ücretsiz
                        Kullanmaya Başla!</NavItem>

                    <br />
                    <br />
                    <small>
                        Soy ağacını araştırmak isteyenler, 1923’ten daha geriye gidildiğinde 1903 yılında yapılan ilk modern nüfus sayımına denk gelebilirler. Ancak bu bilgilerin çoğu Osmanlıca hazırlanan “Atik Nüfus Defterleri”nde mevcut. Nüfus sayımı bilgileri de İstanbul’daki ilçe nüfus müdürlüklerinde ve Ankara’daki Nüfus ve Vatandaşlık İşleri Genel Müdürlüğü’nde yer alıyor. Eğer İstanbul ve Ankara’daki kayıtlara ve aile bilgilerinize ulaşabilirseniz, daha eski kayıtları da bulmak için devam etmek istediğiniz takdirde 1870 tarihli nüfus sayımı karşınıza çıkar. Bu sayıma dair bilgiler de gene İstanbul’daki nüfus il ve ilçe müdürlükleri ile Ankara’da bulunuyor.
                    </small>



                </Container>}


        </div>)
    }

}
import React, {Component} from "react";
import {Button, Container, Header} from "semantic-ui-react";
import animateScrollTo from "animated-scroll-to";

export default () => <Container className="ui text vertical center segment"
                                                 style={{margin: '3em 0'}}>
    <Header as='h2'>Gizlilik Politikamız</Header>
    <p>www.altustsoyagaci.com sitesi olarak kişisel gizlilik haklarınıza saygı duyuyor ve sitemizde geçirdiğiniz süre zarfında bunu sağlamak için çaba sarfediyoruz. Kişisel bilgilerinizin güvenliği ile ilgili açıklamalar aşağıda açıklanmış ve bilginize sunulmuştur.
    </p>
    <Header as='h3'>Kayıt Dosyaları</Header>
    <p>www.altustsoyagaci.com web sitesinin amacı Türkiye Cumhuriyet E-Devlet Kapısı üzerinden edinilen Soy Ağacı bilgisinin düzenli ve görsel şekilde kullanıcıya sunmaktır.  </p>
    <p>Web Sitesi kullanıcının sağladığı bilgileri kendi bünyesinde <strong>saklamaz</strong>. Tamamen tarayıcı tabanlı çalıştığı için verilerinizi sadece kullanıcının kendi bilgisayarında işler.</p>
    <p>Kullanıcının sağladığı verilerin doğruluğu ve sorumluluğu kullanıcıya aittir.</p>
    <p>Dinamik olarak oluşturulan bu içeriği bilgisayarınıza kaydettiğinizde kullanım ve paylaşım hakları kullanıcıya aittir.</p>

    <p>Birçok standard web sunucusunda olduğu gibi siteniz.com'da istatistiksel amaçlı log dosyaları kaydı tutmaktadır. Bu dosyalar; ip adresiniz, internet servis sağlayıcınız, tarayıcınızın özellikleri, işletim sisteminiz ve siteye giriş-çıkış sayfalarınız gibi standard bilgileri içermektedir. Log dosyaları kesinlikle istatistiksel amaçlar dışında kullanılmamakta ve mahremiyetinizi ihlal etmemektedir.Ip adresiniz ve diğer bilgiler, şahsi bilgileriniz ile ilişkilendirilmemektedir.
    </p>


    <Header as='h3'>Reklamlar</Header>
    <p>Sitemizde dışarıdan şirketlerin reklamlarını yayınlamaktayız (Google, v.s.). Bu reklamlar çerez (cookıes) içerebilir ve bu sirketler tarafından çerez bilgileri toplanabilir ve bizim bu bilgiye ulaşmamız mümkün değildir. Biz Google Adsense, vb. şirketler ile çalışmaktayız lütfen onların ilgili sayfalarından gizlilik sözleşmelerini okuyunuz.</p>

    <Header as='h3'>Çerezler (Cookies)</Header>
    <p>Sitemizde dışarıdan şirketlerin reklamlarını yayınlamaktayız (Google, v.s.). Bu reklamlar çerez (cookıes) içerebilir ve bu sirketler tarafından çerez bilgileri toplanabilir ve bizim bu bilgiye ulaşmamız mümkün değildir. Biz Google Adsense, vb. şirketler ile çalışmaktayız lütfen onların ilgili sayfalarından gizlilik sözleşmelerini okuyunuz.</p>

    <Header as='h3'>Dış Bağlantılar</Header>
    <p>www.altustsoyagaci.com sitesi, sayfalarından farklı internet adreslerine bağlantı vermektedir. www.altustsoyagaci.com link verdiği, banner tanıtımını yaptığı sitelerin içeriklerinden veya gizlilik prensiplerinden sorumlu değildir. Burada bahsedilen bağlantı verme işlemi, hukuki olarak “atıfta bulunma” olarak değerlendirilmektedir.</p>

    <Header as='h3'>İletişim</Header>
    <p>
        altustsoyagaci.com sitesinde uygulanan gizlilik politikası ile ilgili; her türlü soru, görüş ve düşüncelerinizi bize <strong>bilgi [ @ ] perisco.xyz</strong> adresinden iletebilirsiniz.
    </p>

</Container>

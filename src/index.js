import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import SiteFooter from './Components/SiteFooter';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<SiteFooter />, document.getElementById('footer'));
registerServiceWorker();

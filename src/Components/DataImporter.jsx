/**
 * Created by Oktay on 2/8/2018.
 */
import React, {Component} from "react";
import {Button, Form, Header, Step, Table, TextArea} from "semantic-ui-react";
import Moment from "react-moment";
import {withRouter} from "react-router-dom";

import slugify from "slugify";

class DataImporter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            importInput: '',
            importInputClone: '',
            secereData: []
        };

        this.continueTo = this.continueTo.bind(this);
    }

    static toDate(dateStr) {
        if (!dateStr) {
            return new Date(0);
        }
        const [day, month, year] = dateStr.split(".");
        return new Date(year, month - 1, day)
    }

    resolveData() {
        let {importInput} = this.state;
        let splitted = importInput.replace(/\s(\d{1,3}\s[EK])\s/gm, "#$1|").split("#");

        let regularData = [];

        splitted.map((data, i) => {
            let resolvedLine = this.resolveLine(data);
            if (resolvedLine) {
                regularData.push(resolvedLine);
            }


        });

        this.setState({
            importInputClone: importInput,
            secereData: regularData
        });


    }

    resolveLine(lineText) {

        lineText = lineText.split("T.C.")[0];
        lineText = lineText.split("AÇIKLAMALAR")[0];
        let mapKeywords = [
            "Annesinin Babasının Babasının Annesinin Babasının Annesi",
            "Annesinin Annesinin Babasının Babasının Babası",
            "Babasının Babasının Babasının Babasının Annesi",
            "Babasının Annesinin Babasının Babasının Babası",
            "Babasının Annesinin Babasının Babasının Annesi",
            "Annesinin Annesinin Babasının Babasının Annesi",
            "Annesinin Annesinin Annesinin Babasının Babası",
            "Annesinin Babasının Babasının Babasının Babası",
            "Annesinin Babasının Babasının Babasının Annesi",
            "Annesinin Babasının Babasının Annesinin Babası",
            "Annesinin Babasının Babasının Annesinin Annesi",
            "Annesinin Babasının Annesinin Babasının Annesi",
            "Annesinin Babasının Annesinin Babasının Babası",
            "Babasının Annesinin Annesinin Annesi",
            "Babasının Annesinin Annesinin Babası",
            "Babasının Annesinin Babasının Annesi",
            "Babasının Annesinin Babasının Babası",
            "Babasının Babasının Babasının Babası",
            "Babasının Babasının Babasının Annesi",
            "Annesinin Annesinin Annesinin Babası",
            "Babasının Babasının Annesinin Babası",
            "Annesinin Babasının Annesinin Babası",
            "Annesinin Annesinin Babasının Babası",
            "Babasının Babasının Annesinin Annesi",
            "Annesinin Babasının Annesinin Annesi",
            "Annesinin Annesinin Babasının Annesi",
            "Annesinin Annesinin Annesinin Annesi",
            "Annesinin Babasının Babasının Annesi",
            "Annesinin Babasının Babasının Babası",
            "Babasının Annesinin Babası",
            "Babasının Annesinin Annesi",
            "Babasının Babasının Annesi",
            "Annesinin Babasının Annesi",
            "Annesinin Babasının Babası",
            "Annesinin Annesinin Babası",
            "Babasının Babasının Babası",
            "Annesinin Annesinin Annesi",
            "Babasının Babası",
            "Annesinin Babası",
            "Babasının Annesi",
            "Annesinin Annesi",
            "Babası",
            "Annesi",
            "Kendisi"];

        let isValid = false;
        let matchedRegex = "";
        let matchedControl = "";
        let lastmatchlength = 0;
        for (let mapKeywordIndex in mapKeywords) {
            let control = mapKeywords[mapKeywordIndex];
            let Regex = new RegExp("(" + control.replace(/\s/g, "\\s*(\\r\\n)*\\s*") + ")", "mgi");

            if (Regex.test(lineText) && control.length > lastmatchlength) {
                isValid = true;
                matchedRegex = Regex;
                matchedControl = control;
                lastmatchlength = control.length;

            }
        }


        if (isValid) {
            lineText = lineText.replace(matchedRegex, "");
            lineText = lineText.replace(/\r\n/g, " ");
            lineText = lineText.replace(/\s+/g, " ");
            lineText = lineText.replace(/(^\s|\s$)/g, " ");
            lineText = lineText.replace(/\/\s+/g, "/");

            let isim = lineText.split("|").pop().split(" ").slice(0, 3).join(" ");
            let cinsiyet = lineText.split("|")[0].replace(/[^EK]*/g, "");
            let sira = lineText.split("|")[0].replace(/[^\d]*/g, "");
            let dogum = "";
            let olum = "";
            let tarihler = lineText.replace(/\//gm,'.').match(/(\d{2}\.\d{2}\.\d{4})/g);
            if (tarihler) {
                dogum = tarihler[0];
                olum = tarihler[1];
            }
            return {
                yakinlik: matchedControl,
                path: slugify(matchedControl).replace(/\-/g, ''),
                raw: lineText,
                isim,
                cinsiyet,
                sira,
                dogum,
                olum,
            };
        }
    }

    continueTo(data) {

        localStorage.setItem("secere",JSON.stringify(data));

        this.props.history.push("/soy-agaci-grafigi")

    }


    render() {
        return (

            <Form>
                <Header as="h2">Sadece Kopyalayın ve Yapıştırın!</Header>
                <p>www.turkiye.gov.tr Adresinden öğrendiğiniz soy ağacı bilginizde yazanları kopyalayıp aşağıdaki kutuya yapıştırmanız yeterli.</p>
                <p>Eğer daha önceden “Belge oluştur” diyerek geçmiş yıllara ait soy ağacını PDF olarak indirdiyseniz. Dosyayı açıp <strong>tüm içeriğini kopyalayıp (Ctrl+A ve Ctrl+C)</strong>, Aşağıdaki metin kutusunu yapıştırın <strong>(Ctrl+V)</strong>.</p>
                <Step.Group ordered style={{width: '100%'}}>
                    <Step completed>
                        <Step.Content>
                            <Step.Title>Belge Oluştur</Step.Title>
                            <Step.Description>E-Devlet Üzerinden</Step.Description>
                        </Step.Content>
                    </Step>

                    <Step className={this.state.importInput.length > 10 ? 'completed' : 'disabled'}>
                        <Step.Content>
                            <Step.Title>Dosya İçeriğini</Step.Title>
                            <Step.Description>Aşağıdaki Alana Yapıştır</Step.Description>
                        </Step.Content>
                    </Step>

                    <Step className={!!this.state.secereData.length ? 'completed' : 'disabled'}>
                        <Step.Content>
                            <Step.Title>Önizleme</Step.Title>
                            <Step.Description>Ve Ağaç Oluştur</Step.Description>
                        </Step.Content>
                    </Step>
                </Step.Group>

                <TextArea ref={obj=>this.props.onTextareaRef(obj)} placeholder='T.C. İÇİŞLERİ BA... ' rows={6} value={this.state.importInput} onChange={(e) => {
                    this.setState({importInput: e.currentTarget.value.replace(/\s+/gm, ' ')})
                }} style={{marginBottom: '2em'}}/>

                {!this.state.secereData.length || this.state.importInput !== this.state.importInputClone ?
                    <Button color={this.state.importInput ? 'blue' : 'grey'}
                            onClick={() => this.resolveData()}>Önizleme</Button> :
                    <Button color="red" onClick={() => this.setState({importInput: '', secereData: ''})}>İptal</Button>}

                {!!this.state.secereData.length && <Button floated="right" color='green' size='large'
                                                           onClick={() => this.continueTo(this.state.secereData)}>Devam
                    Et</Button> }

                {!!this.state.secereData.length &&
                <div style={{marginTop: '3em'}}>
                    <Table className="small" celled inverted selectable>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ANAHTAR</th>
                            <th>İSİM</th>
                            <th>CİNSİYET</th>
                            <th>DOĞUM</th>
                            <th>ÖLÜM</th>
                            <th>YAŞI</th>

                        </tr>
                        </thead>
                        <tbody>
                        {this.state.secereData.map((item, key) =>
                            (<tr key={key}>
                                <td>{item.sira}</td>
                                <td>{item.yakinlik}</td>
                                <td>{item.isim}</td>
                                <td>{item.cinsiyet}</td>
                                <td>{item.dogum}</td>
                                <td>{item.olum}</td>
                                <td>{item.dogum && item.olum &&
                                <Moment diff={DataImporter.toDate(item.dogum)} unit="years"
                                >{DataImporter.toDate(item.olum)}</Moment>}</td>

                            </tr>)
                        )}
                        </tbody>

                    </Table>
                    <Button floated="right" color='green' size='large'
                            onClick={() => this.continueTo(this.state.secereData)}>Devam Et</Button>
                </div>
                }
            </Form>

        );
    }
}
export default withRouter(DataImporter);
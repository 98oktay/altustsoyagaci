/**
 * Created by Oktay on 2/15/2018.
 */
import React, {Component} from "react";
import {Container, Header} from "semantic-ui-react";

export default class SiteHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<Container className="ui vertical center aligned segment"
                           style={{margin: '3em 0', paddingBottom: '3em'}}>
            <Header inverted as='h1'
                    className="center aligned">{this.props.title?this.props.title:"Alt Üst Soy Ağacı"}</Header>
            <p className="ui text inverted">E-Devlet üzerinden kullanıcı adı ve şifrenizle giriş
                yaptıktan sonra Nüfus ve Vatandaşlık İşleri Genel Müdürlüğü altyapısı üzerinden alt-üst
                soy bilgisi sorgulama hizmetini kullanın.
                Ardından <strong>oluşan sayfanın tüm içeriğini kopyalayıp (Ctrl+A ve Ctrl+C)</strong>,
                Aşağıdaki <strong>metin kutusunu yapıştırın</strong>. Ve işte aile Soyağacınız.</p>

        </Container>)
    }
}
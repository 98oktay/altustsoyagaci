/**
 * Created by Oktay on 2/8/2018.
 */
import React, {Component} from "react";
import {Container, Divider, Grid, Header, List, Segment} from "semantic-ui-react";
import "semantic-ui-css/semantic.css";

export default class SiteFooter extends Component {

    render() {
        return (
            <Segment inverted vertical style={{margin: '1em 0em 0em', padding: '1em 0em'}}>
                <Container textAlign='center'>
                    <List horizontal inverted divided link>
                        <List.Item as='a' href='http://www.oktaybaskus.com.tr' target="_blank">2018 &copy; Oktay Başkuş</List.Item>
                        <List.Item as='a' href='/gizlilik'>Gizlilik Politikası</List.Item>
                    </List>
                </Container>
            </Segment>
        );
    }
}
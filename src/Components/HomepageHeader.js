/**
 * Created by Oktay on 2/15/2018.
 */
import React, {Component} from "react";
import {Button, Container, Header} from "semantic-ui-react";
import animateScrollTo from "animated-scroll-to";

let HomepageHeader = () => <Container className="ui vertical center aligned segment"
                                                 style={{margin: '3em 0'}}>
    <Header inverted as='h1' className="center aligned">Nasıl Soyağacı Oluşturulur?</Header>
    <p className="ui text inverted">E-Devlet üzerinden kullanıcı adı ve şifrenizle giriş
        yaptıktan sonra Nüfus ve Vatandaşlık İşleri Genel Müdürlüğü altyapısı üzerinden alt-üst
        soy bilgisi sorgulama hizmetini kullanın.
        Ardından <strong>oluşan sayfanın tüm içeriğini kopyalayıp (Ctrl+A ve Ctrl+C)</strong>,
        Aşağıdaki <strong>metin kutusunu yapıştırın</strong>. Ve işte aile Soyağacınız.</p>

    <Button color="green" style={{margin: "2em 0"}} onClick={() => {
        animateScrollTo(300, {speed: 3000, element: window, cancelOnUserAction: false});
    }} className="huge">Hemen Ücretsiz Kullan!</Button>
</Container>;

export default HomepageHeader;
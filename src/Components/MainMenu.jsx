/**
 * Created by Oktay on 2/8/2018.
 */
import React, {Component} from "react";
import {Dropdown, Menu} from "semantic-ui-react";
import "semantic-ui-css/semantic.css";
import NavItem from "./NavItem";

export default class MainMenu extends Component {


    render() {
        return (
            <Menu inverted>
                <Menu.Item as='a' href="/" header>
                    ALT-ÜST SOYAĞACI OLUŞTUR
                </Menu.Item>
                <NavItem exact={true} to="/" className="item">Anasayfa</NavItem>
                <NavItem to="/gizlilik" className="item right aligned">Gizlilik Politikası</NavItem>
                <Dropdown item simple text='Sosyal Medyadan Takip Edin'>
                    <Dropdown.Menu>
                        <Dropdown.Item href="/">Nasıl Sorgulama Yaparım</Dropdown.Item>
                        <Dropdown.Item href="/">Nasıl Soy Ağacı Oluştururum</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Menu>

        );
    }
}
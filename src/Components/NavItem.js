/**
 * Created by Oktay on 8/5/2017.
 */
import React, {PropTypes} from "react";
import {NavLink, Route} from "react-router-dom";


export default function NavItem({children, to, exact, className}) {
    return (
        <Route path={to} exact={exact} children={({match}) => (
            <NavLink exact={exact} className={className} to={to}>{children}</NavLink>
        )}/>
    )
}

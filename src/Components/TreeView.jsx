/**
 * Created by Oktay on 2/8/2018.
 */
import React, {Component} from "react";
import {Grid} from "semantic-ui-react";
import "semantic-ui-css/semantic.css";
import People from "./People";

export default class TreeView extends Component {
    constructor(props) {
        super(props);


        this.state = {
            secereData: props.secereData
        };

        this.loadNames();

    }

    static toDate(dateStr) {
        if (!dateStr) {
            return new Date(0);
        }
        const [day, month, year] = dateStr.split(".");
        return new Date(year, month - 1, day)
    }

    loadNames() {
        let peoples = {};
        this.state.secereData.map((value) => {
            peoples[value.path] = value;
        });
        this.state.peoples = peoples;
    }

    getItem(path) {

        return this.state.secereData.find((item) => {
            return item.path === path
        });
    }

    render() {
        let peoples = this.state.peoples;

        return (

            <Grid centered columns={2} style={{padding:'0 20px'}}>
                <Grid.Column>
                    <People me info={peoples.Kendisi}/>
                </Grid.Column>



                <Grid.Row centered columns={3}>
                    <Grid.Column>
                        <People className="pink" info={peoples.Annesi}/>
                    </Grid.Column>
                    <Grid.Column>
                        <People info={peoples.Babasi}/>
                    </Grid.Column>
                </Grid.Row>


                <Grid.Row centered columns={2}>
                    <Grid.Column>
                        <Grid centered columns={2}>
                            <Grid.Column>
                                <People className="pink" info={peoples.AnnesininAnnesi}/>
                                <Grid centered columns={2}>
                                    <Grid.Column>
                                        <People info={peoples.AnnesininAnnesininAnnesi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.AnnesininAnnesininAnnesininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.AnnesininAnnesininAnnesininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininAnnesininAnnesininAnnesinBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.AnnesininAnnesininAnnesininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People className="pink" simple info={peoples.AnnesininAnnesininAnnesininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininAnnesininAnnesininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <People info={peoples.AnnesininAnnesininBabasi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink"  info={peoples.AnnesininAnnesininBabasininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink"  info={peoples.AnnesininAnnesininBabasininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininAnnesininBabasininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.AnnesininAnnesininBabasininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink"  info={peoples.AnnesininAnnesininBabasininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininAnnesininBabasininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                </Grid>
                            </Grid.Column>
                            <Grid.Column>
                                <People info={peoples.AnnesininBabasi}/>
                                <Grid centered columns={2}>
                                    <Grid.Column>
                                        <People className="pink" info={peoples.AnnesininBabasininAnnesi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.AnnesininBabasininAnnesininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.AnnesininBabasininAnnesininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininBabsininAnnesininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.AnnesininBabsininAnnesininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.AnnesininBabasininAnnesininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininBabsininAnnesininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <People info={peoples.AnnesininBabasininBabasi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.AnnesininBabasininBabasininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.AnnesininBabasininBabasininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininBabasininBabasininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.AnnesininBabasininBabasininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.AnnesininBabasininBabasininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.AnnesininBabasininBabasininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                </Grid>
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                    <Grid.Column>
                        <Grid centered columns={2}>
                            <Grid.Column>
                                <People className="pink" info={peoples.BabasininAnnesi}/>
                                <Grid centered columns={2}>
                                    <Grid.Column>
                                        <People className="pink" info={peoples.BabasininAnnesininAnnesi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.BabasininAnnesininAnnesininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininAnnesininAnnesininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininAnnesininAnnesininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.BabasininAnnesininAnnesininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininAnnesininAnnesininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininAnnesininAnnesininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <People info={peoples.BabasininAnnesininBabasi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.BabasininAnnesininBabasininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininAnnesininBabasininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininAnnesininBabasininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.BabasininAnnesininBabasininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininAnnesininBabasininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininAnnesininBabasininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                </Grid>
                            </Grid.Column>
                            <Grid.Column>
                                <People info={peoples.BabasininBabasi}/>
                                <Grid centered columns={2}>
                                    <Grid.Column>
                                        <People className="pink" info={peoples.BabasininBabasininAnnesi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.BabasininBabasininAnnesininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininBabasininAnnesininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininBabasininAnnesininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.BabasininBabasininAnnesininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininBabasininAnnesininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininBabasininAnnesininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <People info={peoples.BabasininBabasininBabasi}/>
                                        <Grid centered columns={2}>
                                            <Grid.Column>
                                                <People small className="pink" info={peoples.BabasininBabasininBabasininAnnesi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simpleclassName="pink"  info={peoples.BabasininBabasininBabasininAnnesininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininBabasininBabasininAnnesininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <People small info={peoples.BabasininBabasininBabasininBabasi}/>
                                                <Grid centered columns={2}>
                                                    <Grid.Column>
                                                        <People simple className="pink" info={peoples.BabasininBabasininBabasininBabasininAnnesi}/>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <People simple info={peoples.BabasininBabasininBabasininBabasininBabasi}/>
                                                    </Grid.Column>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid>
                                    </Grid.Column>
                                </Grid>
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                </Grid.Row>


            </Grid>

        );
    }
}
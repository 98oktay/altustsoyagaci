/**
 * Created by Oktay on 2/8/2018.
 */
import React, {Component} from "react";
import {Grid} from "semantic-ui-react";
import "semantic-ui-css/semantic.css";
import Moment from "react-moment";

export default class People extends Component {
    constructor(props) {
        super(props);

        this.state = {
            info: props.info
        };
    }

    toDate(dateStr) {
        if (!dateStr) {
            return new Date(0);
        }
        const [day, month, year] = dateStr.split(".");
        return new Date(year, month - 1, day)
    }

    render() {
        let info = this.state.info;

        if(!info) {
            return (<div className="ui center aligned segment">
                ?
            </div>)
        }

        return (

            <div className={["ui center aligned person segment", this.props.className, this.props.me? 'blue':''].join(' ')} >
                <p className={this.props.simple? "vertical-text": this.props.small?"small":""}>
                {info.isim}
                    {info.dogum && info.olum && <small>(<Moment diff={this.toDate(info.dogum)} unit="years"
                    >{this.toDate(info.olum)}</Moment>) </small>}
                <br />
                {info.dogum}
                </p>
            </div>

        );
    }
}
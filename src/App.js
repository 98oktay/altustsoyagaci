import React, {Component} from "react";
import {BrowserRouter} from "react-router-dom";
import Homepage from "./Pages/Homepage";
import {Route, Switch} from "react-router-dom";
import {Button, Container, Header} from "semantic-ui-react";
import MainMenu from "./Components/MainMenu";
import HomepageHeader from "./Components/HomepageHeader";
import SiteHeader from "./Components/SiteHeader";



import "./App.css";
import Gizlilik from "./Pages/Gizlilik";
import Sonuc from "./Pages/Sonuc";



const NotFound = () =>
    <Container text className="center aligned">
        <h3>404 page not found</h3>
        <p>We are sorry but the page you are looking for does not exist.</p>
    </Container>;


export const Pages = () =>
    <Switch>
        <Route exact path="/" component={Homepage}/>
        <Route exact path="/gizlilik" component={Gizlilik}/>
        <Route exact path="/soy-agaci-grafigi" component={Sonuc}/>
        <Route path="*" component={NotFound}/>
    </Switch>;


export const Headers = () =>
    <Switch>
        <Route exact path="/" component={HomepageHeader}/>
        {/*<Route exact path="/gizlilik"><SiteHeader title="Gizlilik Politikamız" /></Route>*/}
        <Route path="*" ><SiteHeader /></Route>

    </Switch>;





class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div>
                    <div className="app-header">
                        <MainMenu />
                        <Headers/>
                    </div>
                    <Pages/>
                </div>

            </BrowserRouter>
        );
    }
}

export default App;

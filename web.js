/**
 * Created by Oktay on 08/13/2017.
 */


var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

var path = require('path');

var staticRouter = express.Router();


staticRouter.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});



staticRouter.use(function (req, res, next) {
    if (path.extname(req.path).length > 0) {
        // normal static file request
        next();
    }
    else {
        // should force return `index.html` for angular.js
        req.url = '/index.html';
        next();
    }
});

app.use(expressValidator());
app.use(bodyParser.urlencoded({limit: 1024 * 1024 * 10, extended:true}));


app.use(staticRouter);

staticRouter.use(express.static(__dirname + '/build'));

app.listen(8082);

//module.exports = app;